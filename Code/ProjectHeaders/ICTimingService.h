/****************************************************************************
  Header file for ICTimingService
  based on the Gen2 Events and Services Framework
 ****************************************************************************/

#ifndef ICTimingService_H
#define ICTimingService_H

// Event Definitions
#include "ES_Events.h"    /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Port.h"      /* defines serial routines */

// Public Function Prototypes
bool InitICTimingService(uint8_t Priority);
bool PostICTimingService(ES_Event_t ThisEvent);
ES_Event_t RunICTimingService(ES_Event_t ThisEvent);
bool Check4EndOfCapture(void);

#endif /* ICTimingService_H */
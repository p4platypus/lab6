/****************************************************************************
 Module
   ICTimingService.c

 Description
   This is the main service responsible for capturing the timing information
   about the input Morse signal.
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
// This module
#include "../ProjectHeaders/ICTimingService.h"

// Hardware
#include <xc.h>
#include <proc/p32mx170f256b.h>
#include <sys/attribs.h>                              // for ISR macros

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"

/*----------------------------- Module Defines ----------------------------*/
#define MAX_IC_NUM 300                                // input capture limit

/*---------------------------- Module Functions ---------------------------*/
void InitTimer(void);
void InitInputCapture(void);
void StartTimer(void);
void StartInputCapture(void);
void __ISR(_TIMER_2_VECTOR, IPL6SOFT) TimerResponse(void);
void __ISR(_INPUT_CAPTURE_4_VECTOR, IPL7SOFT) InputCaptureResponse(void);

/*---------------------------- Module Variables ---------------------------*/
static uint8_t MyPriority;                            // priority variable

static volatile uint16_t RollOverCount = 0;           // # of elapsed period
static volatile uint32_t CapturedTime[MAX_IC_NUM+5];  // array of captured time
static volatile uint16_t CapturedTimeCount = 0;       // captured time count

static bool EndOfCaptureFlag = false;                 // event checker flag

static uint32_t DotTimeAvg;                           // mean dot time
static uint32_t DotSpaceAvg;                          // mean dot space time
static uint32_t DashTimeAvg;                          // mean dash time
static uint32_t CharSpaceAvg;                         // mean char space time
static uint32_t WordSpaceAvg;                         // mean word space time
static uint16_t DotTimeCount = 0;                     // dot time count
static uint16_t DotSpaceCount = 0;                    // dot space count
static uint16_t DashTimeCount = 0;                    // dash time count
static uint16_t CharSpaceCount = 0;                   // char space count
static uint16_t WordSpaceCount = 0;                   // word space count

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitICTimingService

 Parameters
     uint8_t: the priority of this service

 Returns
     bool: false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition, and does any
     other required initialization for this state machine
****************************************************************************/
bool InitICTimingService(uint8_t Priority)
{
    ES_Event_t ThisEvent;

    // Initialize MyPriority with passed in parameter
    MyPriority = Priority;
    // Disable analog operations on RA0
    ANSELAbits.ANSA0 = 0;
    // Configure RA0 as input line to receive Morse signal
    TRISAbits.TRISA0 = 1;
    // Initialize Timer 2
    InitTimer();
    // Initialize Input Capture 4
    InitInputCapture();
    // Start Timer 2
    StartTimer();
    // Start Input Capture 4
    StartInputCapture();
    printf("Hardware initialization completed\r\n");
    
    // post the initial transition event
    ThisEvent.EventType = ES_INIT;
    if(ES_PostToService(MyPriority, ThisEvent) == true)
        return true;
    else
        return false;
}

/****************************************************************************
 Function
     PostICTimingService

 Parameters
     ES_Event_t: the event to post to the queue

 Returns
     bool: false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this service's queue
****************************************************************************/
bool PostICTimingService(ES_Event_t ThisEvent)
{
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
     RunICTimingService

 Parameters
     ES_Event_t: the event to process

 Returns
     ES_Event_t: ES_NO_EVENT if no error ES_ERROR otherwise

 Description
     Calculates the average dot time, dot space time, dash time, char
     space time, and word space time of the input Morse signal
****************************************************************************/
ES_Event_t RunICTimingService(ES_Event_t ThisEvent)
{
    ES_Event_t ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT;              // assume no errors
    
    switch(ThisEvent.EventType) {
        // If event is END_OF_CAPTURE
        case END_OF_CAPTURE:
            // Disable Timer 2
            T2CONbits.ON = 0;
            // Disable Input Capture 4
            IC4CONbits.ON = 0;
            
            // Calculate tick-to-time conversion factor
            uint32_t TickToTime = 20000000/8/1000;
            // Initialize 5 variables to store running totals of dot-time, 
            // dot-space, dash-time, char-space, and word-space
            uint32_t DotTimeTotal = 0;
            uint32_t DotSpaceTotal = 0;
            uint32_t DashTimeTotal = 0;
            uint32_t CharSpaceTotal = 0;
            uint32_t WordSpaceTotal = 0;
            // Initialize 1 variable to store computed interval of pulse
            uint32_t ComputedInterval;
            
            // For first 500 elements in captured time array
            uint16_t i;
            for(i = 1; i < MAX_IC_NUM; i++) {
                // Subtract previous element from current element
                // to compute interval of pulse
                ComputedInterval =
                        (CapturedTime[i] - CapturedTime[i-1])/TickToTime;
                // If compute interval of pulse is less than 200ms and
                // current element index is odd
                if(ComputedInterval < 200 && i % 2 == 1) {
                    // Increment running total of dot-time and its count 
                    DotTimeTotal += ComputedInterval;
                    DotTimeCount++;
                // If compute interval of pulse is less than 200ms and
                // current element index is even
                } else if(ComputedInterval < 200 && i % 2 == 0) {
                    // Increment running total of dot-space and its count 
                    DotSpaceTotal += ComputedInterval;
                    DotSpaceCount++;
                // If compute interval of pulse is less than 500ms and
                // current element index is odd
                } else if(ComputedInterval < 500 && i % 2 == 1) {
                    // Increment running total of dash-time and its count 
                    DashTimeTotal += ComputedInterval;
                    DashTimeCount++;
                // If compute interval of pulse is less than 500ms and
                // current element index is even
                } else if(ComputedInterval < 500 && i % 2 == 0) {
                    // Increment running total of char-space and its count 
                    CharSpaceTotal += ComputedInterval;
                    CharSpaceCount++;
                // If compute interval of pulse is less than 1050ms and
                // current element index is even
                } else if(ComputedInterval < 1050 && i % 2 == 0) {
                    // Increment running total of word-space and its count 
                    WordSpaceTotal += ComputedInterval;
                    WordSpaceCount++;
                // Otherwise, do nothing (bad data)
                } else {}
            }
            
            // Calculate average dot time, dot space time, dash time,
            // char space time, and word space time
            DotTimeAvg = DotTimeTotal/DotTimeCount;
            DotSpaceAvg = DotSpaceTotal/DotSpaceCount;
            DashTimeAvg = DashTimeTotal/DashTimeCount;
            CharSpaceAvg = CharSpaceTotal/CharSpaceCount;
            WordSpaceAvg = WordSpaceTotal/WordSpaceCount;
            // Print results
            printf("\rAverage Dot Time = %d ms with a count of %d\r\n",
                    DotTimeAvg, DotTimeCount);
            printf("\rAverage Dot Space Time = %d ms with a count of %d\r\n",
                    DotSpaceAvg, DotSpaceCount);
            printf("\rAverage Dash Time = %d ms with a count of %d\r\n",
                    DashTimeAvg, DashTimeCount);
            printf("\rAverage Char Space Time = %d ms with a count of %d\r\n",
                    CharSpaceAvg, CharSpaceCount);
            printf("\rAverage Word Space Time = %d ms with a count of %d\r\n",
                    WordSpaceAvg, WordSpaceCount);
        break;
        
        // Repeat cases as required for relevant events
        default:
            ;
    }
    
    return ReturnEvent;
}

/****************************************************************************
 Function
     Check4EndOfCapture

 Parameters
     None

 Returns
     bool: true if enough input capture data are gathered, false otherwise

 Description
     Checks whether enough input capture data are gathered
****************************************************************************/
bool Check4EndOfCapture(void)
{
    bool ReturnVal = false;
        
    // Post event END_OF_CAPTURE if CapturedTimeIndex reaches MAX_IC_NUM
    // for first time
    if((!EndOfCaptureFlag) && (CapturedTimeCount >= MAX_IC_NUM)) {
        ES_Event_t ThisEvent;
        ThisEvent.EventType = END_OF_CAPTURE;
        ThisEvent.EventParam = 1;
        PostICTimingService(ThisEvent);
        ReturnVal = true;
        EndOfCaptureFlag = true;
        printf("Input capture completed\r\n");
    }

    // Return whether enough input capture data are gathered
    return ReturnVal;
}

/***************************************************************************
 Private Functions
 ***************************************************************************/
/****************************************************************************
 Function
     InitTimer
 
 Parameters
     None
 
 Returns
     None
 
 Description
     Initialize the timer needed for capturing the timing information about
     the input Morse signal with the right pre-scaler and period
****************************************************************************/
void InitTimer(void) {
    // Disable Timer 2
    T2CONbits.ON = 0;
    // Select internal PBCLK clock source
    T2CONbits.TCS = 0;
    // Select 16-bit timer module
    T2CONbits.T32 = 0;
    // Disable gated time accumulation
    T2CONbits.TGATE = 0;
    // Choose 8 as pre-scaler
    T2CONbits.TCKPS2 = 0;
    T2CONbits.TCKPS1 = 1;
    T2CONbits.TCKPS0 = 1;
    // Choose 65535 as period
    PR2 = 0xffff;
    
    // Select multi-vector mode for interrupt
    INTCONbits.MVEC = 1;
    // Enable interrupts globally
    __builtin_enable_interrupts();
    // Set priority of timeout interrupt to second highest
    IPC2bits.T2IP = 6;
    // Set sub-priority of timeout interrupt to highest
    IPC2bits.T2IS = 3;
    // Enable timeout interrupt locally
    IEC0SET = _IEC0_T2IE_MASK;
    
    // Set module level roll-over counter to 0
    RollOverCount = 0;
}

/****************************************************************************
 Function
     InitInputCapture
 
 Parameters
     None
 
 Returns
     None
 
 Description
     Initialize the input capture module needed for capturing the timing
     information about the input Morse signal with the right setup
****************************************************************************/
void InitInputCapture(void) {
    // Map RA0 to Input Capture 4
    IC4R = 0b0000;
    
    // Disable input capture
    IC4CONbits.ON = 0;
    // Select input capture mode to be ?Simple Capture Event mode ? every edge, 
    // specified edge first and every edge thereafter?
    IC4CONbits.ICM2 = 1;
    IC4CONbits.ICM1 = 1;
    IC4CONbits.ICM0 = 0;
    // Choose to interrupt on every capture event
    IC4CONbits.ICI1 = 0;
    IC4CONbits.ICI0 = 0;
    // Choose Timer 2 as counter source
    IC4CONbits.ICTMR = 1;
    // Capture the time with 16-bit timer
    IC4CONbits.C32 = 0;
    // Choose to capture rising edge first
    IC4CONbits.FEDGE = 1;
    
    // Set priority of input capture interrupt to highest
    IPC4bits.IC4IP = 7;
    // Set sub-priority of input capture interrupt to highest
    IPC4bits.IC4IS = 3;
    // Enable input capture interrupt locally
    IEC0SET = _IEC0_IC4IE_MASK;
    
    // Set module level captured time counter to 0
    CapturedTimeCount = 0;
}

/****************************************************************************
 Function
     StartTimer
 
 Parameters
     None
 
 Returns
     None
 
 Description
     Start the timer needed for capturing the timing information about
     the input Morse signal
****************************************************************************/
void StartTimer(void) {
    // Clear any pending interrupt flag on Timer 2
    IFS0CLR = _IFS0_T2IF_MASK;
    // Clear timer count
    TMR2 = 0;
    // Enable timer
    T2CONbits.ON = 1;
}

/****************************************************************************
 Function
     StartInputCapture
 
 Parameters
     None
 
 Returns
     None
 
 Description
     Start the input capture module needed for capturing the timing information
     abut the input Morse signal
****************************************************************************/
void StartInputCapture(void) {
    // Clear any pending interrupt flag on Input Capture 4 
    IFS0CLR = _IFS0_IC4IF_MASK;
    // Enable input capture
    IC4CONbits.ON = 1;
}

/****************************************************************************
 Function
     TimerResponse
 
 Parameters
     None
 
 Returns
     None
 
 Description
     Outlines the service routine when a timer interrupt is triggered
****************************************************************************/
void __ISR(_TIMER_2_VECTOR, IPL6SOFT) TimerResponse(void) {
    // Clear interrupt flag on Timer 2
    IFS0CLR = _IFS0_T2IF_MASK;
    // Increment roll-over counter by 1
    RollOverCount++;
}

/****************************************************************************
 Function
     InputCaptureResponse
 
 Parameters
     None
 
 Returns
     None
 
 Description
     Outlines the service routine when an input capture interrupt is triggered
****************************************************************************/
void __ISR(_INPUT_CAPTURE_4_VECTOR, IPL7SOFT) InputCaptureResponse(void) {
    // Read captured time in buffer of Input Capture 4
    uint16_t CurrentTime = IC4BUF;
    // Clear interrupt flag on Input Capture 4
    IFS0CLR = _IFS0_IC4IF_MASK;
    
    // If Timer 2 has pending interrupt and captured time is after roll-over
    if(IFS0bits.T2IF == 1 && CurrentTime < 0x8000) {
        // Increment roll-over counter by 1
        RollOverCount++;
        // Clear interrupt flag on Timer 2
        IFS0CLR = _IFS0_T2IF_MASK;
    }
    
    // Combine roll-over counter with captured time
    uint32_t CombinedTime = ((uint32_t)RollOverCount << 16) | CurrentTime;
    // Record result into captured time array
    CapturedTime[CapturedTimeCount] = CombinedTime;
    // Increment array index by 1
    CapturedTimeCount++;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
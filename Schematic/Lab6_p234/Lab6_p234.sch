EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Notes Line
	7400 1350 3550 1350
Wire Notes Line
	7400 4100 7400 1350
Wire Notes Line
	3550 4100 7400 4100
Wire Notes Line
	3550 1350 3550 4100
Wire Wire Line
	6850 3000 8800 3000
Connection ~ 2400 3200
Connection ~ 8450 3750
Connection ~ 8450 2300
Wire Wire Line
	9650 4450 9650 4800
Connection ~ 9350 4800
Wire Wire Line
	9350 4450 9350 4800
Wire Wire Line
	9350 4900 9350 4800
$Comp
L power:GNDREF #PWR?
U 1 1 60173657
P 9350 4900
F 0 "#PWR?" H 9350 4650 50  0001 C CNN
F 1 "GNDREF" H 9355 4727 50  0000 C CNN
F 2 "" H 9350 4900 50  0001 C CNN
F 3 "" H 9350 4900 50  0001 C CNN
	1    9350 4900
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Cap C5
U 1 1 60172D03
P 9500 4800
F 0 "C5" V 9248 4800 50  0000 C CNN
F 1 "10uF" V 9339 4800 50  0000 C CNN
F 2 "" H 9538 4650 50  0001 C CNN
F 3 "" H 9500 4800 50  0001 C CNN
	1    9500 4800
	0    1    1    0   
$EndComp
Connection ~ 8450 4050
Wire Wire Line
	8450 4150 8450 4050
$Comp
L power:GNDREF #PWR?
U 1 1 6017201B
P 8450 4150
F 0 "#PWR?" H 8450 3900 50  0001 C CNN
F 1 "GNDREF" H 8455 3977 50  0000 C CNN
F 2 "" H 8450 4150 50  0001 C CNN
F 3 "" H 8450 4150 50  0001 C CNN
	1    8450 4150
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 60172015
P 8450 3650
F 0 "#PWR?" H 8450 3500 50  0001 C CNN
F 1 "+3.3V" H 8465 3823 50  0000 C CNN
F 2 "" H 8450 3650 50  0001 C CNN
F 3 "" H 8450 3650 50  0001 C CNN
	1    8450 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 3750 8450 3650
Wire Wire Line
	8450 4050 8800 4050
Wire Wire Line
	8450 3750 8800 3750
$Comp
L ME218_BaseLib:Cap C4
U 1 1 6017200C
P 8450 3900
F 0 "C4" H 8565 3946 50  0000 L CNN
F 1 "0.1uF" H 8565 3855 50  0000 L CNN
F 2 "" H 8488 3750 50  0001 C CNN
F 3 "" H 8450 3900 50  0001 C CNN
	1    8450 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 2300 10550 2300
$Comp
L power:+3.3V #PWR?
U 1 1 6016FD9C
P 10550 1900
F 0 "#PWR?" H 10550 1750 50  0001 C CNN
F 1 "+3.3V" H 10565 2073 50  0000 C CNN
F 2 "" H 10550 1900 50  0001 C CNN
F 3 "" H 10550 1900 50  0001 C CNN
	1    10550 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 2000 10550 1900
$Comp
L ME218_BaseLib:Res1 R7
U 1 1 6016F15A
P 10550 2150
F 0 "R7" H 10618 2196 50  0000 L CNN
F 1 "10k" H 10618 2105 50  0000 L CNN
F 2 "" V 10590 2140 50  0001 C CNN
F 3 "" H 10550 2150 50  0001 C CNN
	1    10550 2150
	1    0    0    -1  
$EndComp
Connection ~ 8450 2600
Wire Wire Line
	8450 2700 8450 2600
$Comp
L power:GNDREF #PWR?
U 1 1 6016D6C5
P 8450 2700
F 0 "#PWR?" H 8450 2450 50  0001 C CNN
F 1 "GNDREF" H 8455 2527 50  0000 C CNN
F 2 "" H 8450 2700 50  0001 C CNN
F 3 "" H 8450 2700 50  0001 C CNN
	1    8450 2700
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 6016CCDA
P 8450 2200
F 0 "#PWR?" H 8450 2050 50  0001 C CNN
F 1 "+3.3V" H 8465 2373 50  0000 C CNN
F 2 "" H 8450 2200 50  0001 C CNN
F 3 "" H 8450 2200 50  0001 C CNN
	1    8450 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 2300 8450 2200
Wire Wire Line
	8450 2600 8800 2600
Wire Wire Line
	8450 2300 8800 2300
$Comp
L ME218_BaseLib:Cap C3
U 1 1 6016AF70
P 8450 2450
F 0 "C3" H 8565 2496 50  0000 L CNN
F 1 "0.1uF" H 8565 2405 50  0000 L CNN
F 2 "" H 8488 2300 50  0001 C CNN
F 3 "" H 8450 2450 50  0001 C CNN
	1    8450 2450
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:PIC32MX170F256B U3
U 1 1 60169774
P 9850 3150
F 0 "U3" H 9575 4265 50  0000 C CNN
F 1 "PIC32MX170F256B" H 9575 4174 50  0000 C CNN
F 2 "" H 9850 3150 50  0001 C CNN
F 3 "" H 9850 3150 50  0001 C CNN
	1    9850 3150
	1    0    0    -1  
$EndComp
Text Notes 1050 3300 0    50   ~ 0
Vref
Wire Wire Line
	1800 3300 1250 3300
Wire Wire Line
	1250 2500 1250 2300
Connection ~ 1650 2500
Wire Wire Line
	1250 2500 1650 2500
Wire Wire Line
	1250 1900 1250 1800
$Comp
L power:+5V #PWR?
U 1 1 60159CDF
P 1250 1800
F 0 "#PWR?" H 1250 1650 50  0001 C CNN
F 1 "+5V" H 1265 1973 50  0000 C CNN
F 2 "" H 1250 1800 50  0001 C CNN
F 3 "" H 1250 1800 50  0001 C CNN
	1    1250 1800
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Photo_NPN Q1
U 1 1 601596C1
P 1150 2100
F 0 "Q1" H 1340 2146 50  0000 L CNN
F 1 "LTR3208E" H 1340 2055 50  0000 L CNN
F 2 "" H 1350 2200 50  0001 C CNN
F 3 "" H 1150 2100 50  0001 C CNN
	1    1150 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 2500 1900 2500
Wire Wire Line
	1650 3100 1650 2500
Wire Wire Line
	1800 3100 1650 3100
Wire Wire Line
	2400 2500 2400 3200
Wire Wire Line
	2200 2500 2400 2500
Wire Wire Line
	2000 3600 2000 3500
$Comp
L power:GNDREF #PWR?
U 1 1 60156E6E
P 2000 3600
F 0 "#PWR?" H 2000 3350 50  0001 C CNN
F 1 "GNDREF" H 2005 3427 50  0000 C CNN
F 2 "" H 2000 3600 50  0001 C CNN
F 3 "" H 2000 3600 50  0001 C CNN
	1    2000 3600
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60156760
P 2000 2900
F 0 "#PWR?" H 2000 2750 50  0001 C CNN
F 1 "+5V" H 2015 3073 50  0000 C CNN
F 2 "" H 2000 2900 50  0001 C CNN
F 3 "" H 2000 2900 50  0001 C CNN
	1    2000 2900
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Res1 R1
U 1 1 60155055
P 2050 2500
F 0 "R1" V 1845 2500 50  0000 C CNN
F 1 "20k" V 1936 2500 50  0000 C CNN
F 2 "" V 2090 2490 50  0001 C CNN
F 3 "" H 2050 2500 50  0001 C CNN
	1    2050 2500
	0    1    1    0   
$EndComp
$Comp
L ME218_BaseLib:MCP6294 U1
U 3 1 60154588
P 2100 3200
F 0 "U1" H 2444 3246 50  0000 L CNN
F 1 "MCP6294" H 2444 3155 50  0000 L CNN
F 2 "" H 2050 3300 50  0000 C CNN
F 3 "" H 2150 3400 50  0000 C CNN
	3    2100 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 1850 6050 1700
Connection ~ 6050 2300
Wire Wire Line
	6050 2150 6050 2300
$Comp
L ME218_BaseLib:Res1 R5
U 1 1 6014C9C5
P 6050 2000
F 0 "R5" H 6118 2046 50  0000 L CNN
F 1 "10k" H 6118 1955 50  0000 L CNN
F 2 "" V 6090 1990 50  0001 C CNN
F 3 "" H 6050 2000 50  0001 C CNN
	1    6050 2000
	1    0    0    -1  
$EndComp
Connection ~ 6850 2300
Wire Wire Line
	6850 2150 6850 2300
Wire Wire Line
	6600 2300 6850 2300
Wire Wire Line
	6050 2300 6300 2300
Wire Wire Line
	6850 2300 6850 3000
Wire Wire Line
	6050 2900 6050 2300
Wire Wire Line
	6250 2900 6050 2900
$Comp
L ME218_BaseLib:MCP6546 U2
U 1 1 600670FE
P 6550 3000
F 0 "U2" H 6894 3046 50  0000 L CNN
F 1 "MCP6546" H 6894 2955 50  0000 L CNN
F 2 "" H 6500 3100 50  0001 C CNN
F 3 "" H 6600 3200 50  0001 C CNN
	1    6550 3000
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Res1 Rpu
U 1 1 60068504
P 6850 2000
F 0 "Rpu" H 6918 2046 50  0000 L CNN
F 1 "5.1k" H 6918 1955 50  0000 L CNN
F 2 "" V 6890 1990 50  0001 C CNN
F 3 "" H 6850 2000 50  0001 C CNN
	1    6850 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 600694E8
P 6850 1750
F 0 "#PWR?" H 6850 1600 50  0001 C CNN
F 1 "+3.3V" H 6865 1923 50  0000 C CNN
F 2 "" H 6850 1750 50  0001 C CNN
F 3 "" H 6850 1750 50  0001 C CNN
	1    6850 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 1850 6850 1750
Connection ~ 6850 3000
Connection ~ 5500 3100
Wire Wire Line
	5500 3100 6250 3100
Text Notes 5950 1650 0    50   ~ 0
Vref
Text Notes 3700 2400 0    50   ~ 0
Vref
Text Notes 4450 3900 0    50   ~ 0
Vref
Connection ~ 5500 2400
Wire Wire Line
	5500 2000 5500 2400
Wire Wire Line
	5300 2000 5500 2000
Wire Wire Line
	4800 2000 5000 2000
Wire Wire Line
	4800 2400 4800 2000
Wire Wire Line
	4300 2400 3900 2400
Wire Wire Line
	4800 2400 5000 2400
Connection ~ 4800 2400
Wire Wire Line
	4800 2400 4600 2400
$Comp
L ME218_BaseLib:Res1 R3
U 1 1 60123482
P 4450 2400
F 0 "R3" V 4245 2400 50  0000 C CNN
F 1 "10k" V 4336 2400 50  0000 C CNN
F 2 "" V 4490 2390 50  0001 C CNN
F 3 "" H 4450 2400 50  0001 C CNN
	1    4450 2400
	0    1    1    0   
$EndComp
$Comp
L ME218_BaseLib:Cap C2
U 1 1 60122B23
P 5150 2000
F 0 "C2" V 5402 2000 50  0000 C CNN
F 1 "0.01uF" V 5311 2000 50  0000 C CNN
F 2 "" H 5188 1850 50  0001 C CNN
F 3 "" H 5150 2000 50  0001 C CNN
	1    5150 2000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5500 2400 5500 3100
Wire Wire Line
	5300 2400 5500 2400
Wire Wire Line
	4800 3000 4800 2400
Wire Wire Line
	4900 3000 4800 3000
$Comp
L ME218_BaseLib:Res1 R4
U 1 1 601213F9
P 5150 2400
F 0 "R4" V 4945 2400 50  0000 C CNN
F 1 "100k" V 5036 2400 50  0000 C CNN
F 2 "" V 5190 2390 50  0001 C CNN
F 3 "" H 5150 2400 50  0001 C CNN
	1    5150 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 3600 4550 3800
Wire Wire Line
	5100 3500 5100 3400
$Comp
L power:GNDREF #PWR?
U 1 1 6011F8D5
P 5100 3500
F 0 "#PWR?" H 5100 3250 50  0001 C CNN
F 1 "GNDREF" H 5105 3327 50  0000 C CNN
F 2 "" H 5100 3500 50  0001 C CNN
F 3 "" H 5100 3500 50  0001 C CNN
	1    5100 3500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 601203E1
P 5100 2800
F 0 "#PWR?" H 5100 2650 50  0001 C CNN
F 1 "+5V" H 5115 2973 50  0000 C CNN
F 2 "" H 5100 2800 50  0001 C CNN
F 3 "" H 5100 2800 50  0001 C CNN
	1    5100 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3200 4900 3200
Connection ~ 4550 3200
Wire Wire Line
	4550 3300 4550 3200
Wire Wire Line
	4350 3200 4550 3200
$Comp
L ME218_BaseLib:MCP6294 U1
U 2 1 6011DFB0
P 5200 3100
F 0 "U1" H 5544 3146 50  0000 L CNN
F 1 "MCP6294" H 5544 3055 50  0000 L CNN
F 2 "" H 5150 3200 50  0000 C CNN
F 3 "" H 5250 3300 50  0000 C CNN
	2    5200 3100
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Res1 R2
U 1 1 6011D0EE
P 4550 3450
F 0 "R2" H 4618 3496 50  0000 L CNN
F 1 "10k" H 4618 3405 50  0000 L CNN
F 2 "" V 4590 3440 50  0001 C CNN
F 3 "" H 4550 3450 50  0001 C CNN
	1    4550 3450
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Cap C1
U 1 1 6011C566
P 4200 3200
F 0 "C1" V 3948 3200 50  0000 C CNN
F 1 "0.1uF" V 4039 3200 50  0000 C CNN
F 2 "" H 4238 3050 50  0001 C CNN
F 3 "" H 4200 3200 50  0001 C CNN
	1    4200 3200
	0    1    1    0   
$EndComp
$Comp
L ME218_BaseLib:Res1 R6
U 1 1 6006AC7F
P 6450 2300
F 0 "R6" V 6245 2300 50  0000 C CNN
F 1 "100k" V 6336 2300 50  0000 C CNN
F 2 "" V 6490 2290 50  0001 C CNN
F 3 "" H 6450 2300 50  0001 C CNN
	1    6450 2300
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 3400 6450 3300
$Comp
L power:GNDREF #PWR?
U 1 1 6006A166
P 6450 3400
F 0 "#PWR?" H 6450 3150 50  0001 C CNN
F 1 "GNDREF" H 6455 3227 50  0000 C CNN
F 2 "" H 6450 3400 50  0001 C CNN
F 3 "" H 6450 3400 50  0001 C CNN
	1    6450 3400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60067EB7
P 6450 2700
F 0 "#PWR?" H 6450 2550 50  0001 C CNN
F 1 "+5V" H 6465 2873 50  0000 C CNN
F 2 "" H 6450 2700 50  0001 C CNN
F 3 "" H 6450 2700 50  0001 C CNN
	1    6450 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 3200 4050 3200
Text Notes 3600 1500 0    50   ~ 0
Part 3.1
Wire Wire Line
	1250 5200 1250 5100
$Comp
L power:+5V #PWR?
U 1 1 601BCDC5
P 1250 5100
F 0 "#PWR?" H 1250 4950 50  0001 C CNN
F 1 "+5V" H 1265 5273 50  0000 C CNN
F 2 "" H 1250 5100 50  0001 C CNN
F 3 "" H 1250 5100 50  0001 C CNN
	1    1250 5100
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Photo_NPN Q1
U 1 1 601BCDCB
P 1150 5400
F 0 "Q1" H 1340 5446 50  0000 L CNN
F 1 "LTR3208E" H 1340 5355 50  0000 L CNN
F 2 "" H 1350 5500 50  0001 C CNN
F 3 "" H 1150 5400 50  0001 C CNN
	1    1150 5400
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Res1 R8
U 1 1 601BDF62
P 1250 6150
F 0 "R8" H 1318 6196 50  0000 L CNN
F 1 "5.1k" H 1318 6105 50  0000 L CNN
F 2 "" V 1290 6140 50  0001 C CNN
F 3 "" H 1250 6150 50  0001 C CNN
	1    1250 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 6400 1250 6300
$Comp
L power:GNDREF #PWR?
U 1 1 601BE942
P 1250 6400
F 0 "#PWR?" H 1250 6150 50  0001 C CNN
F 1 "GNDREF" H 1255 6227 50  0000 C CNN
F 2 "" H 1250 6400 50  0001 C CNN
F 3 "" H 1250 6400 50  0001 C CNN
	1    1250 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 5600 1250 5800
Wire Wire Line
	1250 5800 2000 5800
Connection ~ 1250 5800
Wire Wire Line
	1250 5800 1250 6000
Text Notes 2050 5800 0    50   ~ 0
Vout
Wire Notes Line
	800  4100 3000 4100
Wire Notes Line
	3000 4100 3000 1350
Wire Notes Line
	3000 1350 800  1350
Wire Notes Line
	800  1350 800  4100
Wire Notes Line
	11000 5300 11000 1350
Wire Notes Line
	11000 1350 8000 1350
Wire Notes Line
	8000 1350 8000 5300
Wire Notes Line
	8000 5300 11000 5300
Text Notes 850  1500 0    50   ~ 0
Part 2.4
Text Notes 8050 1500 0    50   ~ 0
Part 4.1
Wire Notes Line
	800  6800 3000 6800
Wire Notes Line
	3000 6800 3000 4550
Wire Notes Line
	3000 4550 800  4550
Wire Notes Line
	800  4550 800  6800
Text Notes 850  4700 0    50   ~ 0
Part 2.1
$Comp
L ME218_BaseLib:MCP6294 U1
U 4 1 601CA46C
P 5250 5700
F 0 "U1" H 5594 5746 50  0000 L CNN
F 1 "MCP6294" H 5594 5655 50  0000 L CNN
F 2 "" H 5200 5800 50  0000 C CNN
F 3 "" H 5300 5900 50  0000 C CNN
	4    5250 5700
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Res1 R9
U 1 1 601CB15C
P 4450 5500
F 0 "R9" H 4518 5546 50  0000 L CNN
F 1 "10k" H 4518 5455 50  0000 L CNN
F 2 "" V 4490 5490 50  0001 C CNN
F 3 "" H 4450 5500 50  0001 C CNN
	1    4450 5500
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Res1 R10
U 1 1 601CBF91
P 4450 6050
F 0 "R10" H 4518 6096 50  0000 L CNN
F 1 "10k" H 4518 6005 50  0000 L CNN
F 2 "" V 4490 6040 50  0001 C CNN
F 3 "" H 4450 6050 50  0001 C CNN
	1    4450 6050
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Cap_Pol C6
U 1 1 601CF728
P 3900 6050
F 0 "C6" H 4015 6096 50  0000 L CNN
F 1 "10uF" H 4015 6005 50  0000 L CNN
F 2 "" H 3900 6050 50  0001 C CNN
F 3 "" H 3900 6050 50  0001 C CNN
	1    3900 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 5800 4450 5800
Wire Wire Line
	4450 5800 4450 5900
Wire Wire Line
	4450 5800 4450 5650
Connection ~ 4450 5800
$Comp
L power:+5V #PWR?
U 1 1 601DCA78
P 5150 5400
F 0 "#PWR?" H 5150 5250 50  0001 C CNN
F 1 "+5V" H 5165 5573 50  0000 C CNN
F 2 "" H 5150 5400 50  0001 C CNN
F 3 "" H 5150 5400 50  0001 C CNN
	1    5150 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 6100 5150 6000
$Comp
L power:GNDREF #PWR?
U 1 1 601E2150
P 5150 6100
F 0 "#PWR?" H 5150 5850 50  0001 C CNN
F 1 "GNDREF" H 5155 5927 50  0000 C CNN
F 2 "" H 5150 6100 50  0001 C CNN
F 3 "" H 5150 6100 50  0001 C CNN
	1    5150 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 6400 4450 6300
$Comp
L power:GNDREF #PWR?
U 1 1 601E386E
P 4450 6400
F 0 "#PWR?" H 4450 6150 50  0001 C CNN
F 1 "GNDREF" H 4455 6227 50  0000 C CNN
F 2 "" H 4450 6400 50  0001 C CNN
F 3 "" H 4450 6400 50  0001 C CNN
	1    4450 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 6200 3900 6300
Wire Wire Line
	3900 6300 4450 6300
Wire Wire Line
	4450 6200 4450 6300
Connection ~ 4450 6300
Wire Wire Line
	3900 5900 3900 5800
Wire Wire Line
	3900 5800 4450 5800
$Comp
L power:+5V #PWR?
U 1 1 601EB32C
P 4450 5250
F 0 "#PWR?" H 4450 5100 50  0001 C CNN
F 1 "+5V" H 4465 5423 50  0000 C CNN
F 2 "" H 4450 5250 50  0001 C CNN
F 3 "" H 4450 5250 50  0001 C CNN
	1    4450 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 5350 4450 5250
Wire Wire Line
	4850 5100 5550 5100
Wire Wire Line
	5550 5100 5550 5700
Wire Wire Line
	4850 5100 4850 5600
Wire Wire Line
	5550 5700 6200 5700
Connection ~ 5550 5700
Wire Wire Line
	4850 5600 4950 5600
Text Notes 6250 5700 0    50   ~ 0
Vref
Wire Notes Line
	3550 4550 3550 6800
Text Notes 3600 4700 0    50   ~ 0
Vref
Wire Notes Line
	6650 4550 3550 4550
Wire Notes Line
	3550 6800 6650 6800
Wire Notes Line
	6650 4550 6650 6800
$EndSCHEMATC
